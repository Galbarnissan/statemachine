
class Event(object):

    def __init__(self, event_name, event_data=None):
        """

        :param event_name: String.
        :param event_data: Additional data.
        """
        self.event_name = event_name
        self.event_data = event_data
