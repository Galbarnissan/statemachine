
class StateMachineInvalidDuplicatedNameException(Exception):
    pass


class StateMachineInvalidMissingStateForNameException(Exception):
    def __init__(self, name_of_missing_state):
        self.name_of_missing_state = name_of_missing_state


class StateMachine(object):

    @staticmethod
    def _init_states_by_name(states):
        states_by_name = {state.name: state for state in states}
        if len(states_by_name) is not len(states):
            raise StateMachineInvalidDuplicatedNameException
        return states_by_name

    @staticmethod
    def _get_state_by_name(states_by_name, current_state_name):
        current_state = states_by_name.get(current_state_name, None)
        if current_state is None:
            raise StateMachineInvalidMissingStateForNameException(current_state)
        return current_state

    def __init__(self, states, initial_state_name):
        """

        :param states: list of State.
        :param initial_state_name:
        """
        self._states_by_name = StateMachine._init_states_by_name(states)
        self._current_state = StateMachine._get_state_by_name(self._states_by_name, initial_state_name)

    def set_current_state(self, current_state_name):
        """
        To manually update the FSM state.

        :param current_state_name:
        :return: None
        """
        self._current_state = StateMachine._get_state_by_name(self._states_by_name, current_state_name)

    def process_single_event(self, event):
        """
        Single event processing.

        :param event:
        :return: None
        """
        next_state_name = self._current_state.process_event(event)
        self._current_state = StateMachine._get_state_by_name(
            states_by_name=self._states_by_name,
            current_state_name=next_state_name
        )

    def process_events(self, events):
        """
        Multiple events processing.
        :param events: list of iterator of events
        :return: None
        """
        for event in events:
            self.process_single_event(event)

    @property
    def current_state(self):
        return self._current_state

    def dump(self, file_path):
        """

        :param file_path: the persistent file path to dump to
        :return: None
        """
        pass

    @classmethod
    def load(cls, file_path):
        """

        :param file_path: the persistent file path to load from
        :return: None
        """
        pass

