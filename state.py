
class State(object):

    @property
    def name(self):
        """
        Name should be unique.

        :return: the state name
        """
        raise NotImplementedError()

    def process_event(self, event):
        """

        :param event: Type of Event.
        :return: next state name
        """
        raise NotImplementedError()

    @property
    def accept_state(self):
        """
        Optional method to enable acceptors state machines.
        :return: Boolean.
        """
        return True

    def dumps(self):
        """
        :return: string. Dumped state serialization
        """
        pass

    @classmethod
    def loads(cls, data):
        """

        :param data: string. loads the instance.
        :return: new State
        """
        pass
