from state_machine import StateMachine
from state import State
from event import Event


class State0(State):
    @property
    def name(self):
        return "0"

    def process_event(self, event):
        if event.event_name == "a":
            return "1_a"
        else:
            return "1_b"


class State1A(State):
    @property
    def name(self):
        return "1_a"

    def process_event(self, event):
        if event.event_name == "a":
            return "2_a"
        else:
            return "0"


class State2A(State):
    @property
    def name(self):
        return "2_a"

    def process_event(self, event):
        if event.event_name == "a":
            print("WARNING - 3 consecutive events of the same type")
            return "end"
        else:
            return "0"


class State1B(State):
    @property
    def name(self):
        return "1_b"

    def process_event(self, event):
        if event.event_name == "b":
            return "2_b"
        else:
            return "0"


class State2B(State):
    @property
    def name(self):
        return "2_b"

    def process_event(self, event):
        if event.event_name == "b":
            print("WARNING - 3 consecutive events of the same type")
            return "end"
        else:
            return "0"


class StateEnd(State):
    @property
    def name(self):
        return "end"

    def process_event(self, event):
        return "end"


def example_fsm():
    consecutive_event_fsm = StateMachine(
        states=[State0(), State1A(), State2A(), State1B(), State2B(), StateEnd()],
        initial_state_name="0"
    )
    consecutive_event_fsm.process_events([Event(event_name=c) for c in "abaabaaa"])
    consecutive_event_fsm.set_current_state("0")
    consecutive_event_fsm.process_events([Event(event_name=c) for c in "babbabbb"])
    consecutive_event_fsm.set_current_state("0")
    consecutive_event_fsm.process_events([Event(event_name=c) for c in "abababab"])


if __name__ == '__main__':
    example_fsm()
